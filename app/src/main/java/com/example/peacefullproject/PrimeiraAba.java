package com.example.peacefullproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class PrimeiraAba extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.primeira_aba);
    }

    public void newActivity(View view){

        Intent i = new Intent(this, SegundaAba.class);
        startActivity(i);

    }

}
