package com.example.peacefullproject;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class SetimaAba extends AppCompatActivity {

    TextView resultado;
    TextView resultado2;
    int resultadoFinal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setima_aba);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        resultado = findViewById(R.id.resutadoTextView);
        resultado2 = findViewById(R.id.resutado2TextView);

        resultadoFinal = SegundaAba.valorFinal/5;

        resultado.setText("Resultado: " + String.valueOf(resultadoFinal));

        if(resultadoFinal <= 15){
            resultado2.setText("Toda a equipe do Peaceful Project\n orienta que você procure\n URGENTE a ajuda de um psicólogo");
        }else if(resultadoFinal <= 29){
            resultado2.setText("Aparetemente você precisa\n fazer coisas que te alegrem mais");
        } else{
            resultado2.setText("Parabéns!\n Você é uma pessoa feliz.\n Aproveite e faça outras pessoas felizes também");
        }

    }





}
