package com.example.peacefullproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import java.text.NumberFormat;

public class SextaAba extends AppCompatActivity {

    SeekBar seekBar;
    TextView indiceFelicidadeTextViewSeekBar;
    int valor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sexta_aba);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        seekBar = findViewById(R.id.taxaSeekBar);
        indiceFelicidadeTextViewSeekBar = findViewById(R.id.indiceFelicidadeTextViewSeekBar);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                sincronizaTextView();
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    public void sincronizaTextView(){

        valor = seekBar.getProgress();
        SegundaAba.valorFinal =+ valor;
        this.indiceFelicidadeTextViewSeekBar.setText(String.valueOf(valor/10));

    }

    public void newActivity(View view){
        Intent i = new Intent(this, SetimaAba.class);
        startActivity(i);
    }

    public int getValorFelicidade(){
        return valor;
    }
}

